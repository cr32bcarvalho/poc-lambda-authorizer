import json
import os
import urllib.request
import time
from .decode import decode_token

app_client_id = os.environ['CLIENT_ID']

def verify_token(token): 
    claims = decode_token(token)
    current_time = int(time.time())

    if 'error' in claims:
        return claims

    if current_time > claims['exp']:
        return {'error': True, 'message': 'Token is expired'}

    if 'client_id' in claims and claims['client_id'] == app_client_id:
        return claims

    return {'error': True, 'message': 'Token verification failed'}
        
