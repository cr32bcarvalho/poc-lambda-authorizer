import json
import os
import urllib.request
import time
from jose import jwk, jwt
from jose.utils import base64url_decode

region = os.environ['REGION']
userpool_id =  os.environ['USER_POOL_ID']
keys_url = 'https://cognito-idp.{}.amazonaws.com/{}/.well-known/jwks.json'.format(region, userpool_id)

with urllib.request.urlopen(keys_url) as f:
  response = f.read()
keys = json.loads(response.decode('utf-8'))['keys']

def decode_token(token):

    headers = jwt.get_unverified_headers(token)
    kid = headers['kid']
    key_index = -1

    for i in range(len(keys)):
        if kid == keys[i]['kid']:
            key_index = i
            break

    if key_index == -1:
        return {'error': True, 'message': 'Public key not found '}

    public_key = jwk.construct(keys[key_index])
   
    message, encoded_signature = str(token).rsplit('.', 1)
   
    decoded_signature = base64url_decode(encoded_signature.encode('utf-8'))
    
    if not public_key.verify(message.encode("utf8"), decoded_signature):
        return {'error': True, 'message': 'Signature verification failed'}

    return jwt.get_unverified_claims(token)
        
